---
title: "About"
date: 2019-06-29T15:17:36+07:00
type: "page"
---

[comment]: YYYY-MM-DDThh:mm:ssZZZ

### Mission

ความตั้งใจของเพจนี้ คือการแบ่งปันประสบการณ์เกี่ยวกับการทำ startup โดยจะเน้นไปในเรื่อง technical แนว ๆ software development แต่ก็จะสอดแทรกแนวคิดธุรกิจไปในตัวด้วยนะครับ

---

### เกี่ยวกับผู้เขียน

#### กมล เฉลิมวิริยะ - **Kamol Chalermviriya**

#### คำนิยามตัวเอง

ผู้แก้ปัญหาระดับองค์กรคนหนึ่ง, นักพัฒนาซอฟต์แวร์คนหนึ่ง, เป็นสามีคนหนึ่ง และเป็นพ่อคนหนึ่งของลูกสาวคนหนึ่ง อุทิศตนให้กับการแก้ปัญหาที่ท้าทายโดยใช้การพัฒนาซอฟต์แวร์และการโค้ช

An organizational problem solver, a software developer, a husband, and a father of a daughter. Dedicated to solving challenging problems using software development and coaching.

#### ประวัติย่อ

จบมัธยมปลายจาก [โรงเรียนไตรมิตรวิทยาลัย](https://th.wikipedia.org/wiki/%E0%B9%82%E0%B8%A3%E0%B8%87%E0%B9%80%E0%B8%A3%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B9%84%E0%B8%95%E0%B8%A3%E0%B8%A1%E0%B8%B4%E0%B8%95%E0%B8%A3%E0%B8%A7%E0%B8%B4%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%A5%E0%B8%B1%E0%B8%A2) และได้เข้าศึกษาต่อ ปริญญาตรี [วิศวกรรมศาสตร์](https://www.eng.chula.ac.th/en/) ([Intania 85](https://www.facebook.com/groups/129263700582/)) [สาขาวิศวกรรมคอมพิวเตอร์](https://www.cp.eng.chula.ac.th/en/) (รุ่น CP28) จุฬาลงกรณ์มหาวิทยาลัย เรียน ๆ เล่น ๆ ได้ เกียรตินิยมอันดับสอง (แหะ ๆ จริง ๆ ก็มีตั้งใจเรียนบ้างแหละ เพื่อน ๆ ที่ได้เกียรตินิยมอันดับหนึ่งนี่ เก่งเกินคนกันจริง ๆ)

เข้าทำงานที่แรกที่ Thomson Reuters Software (ปัจจุบันเปลี่ยนชื่อเป็น [refinitiv](https://www.refinitiv.com/en)) ทำไปได้ประมาณ 2 ปี ก็ขอทุนศึกษาต่อนอกเวลากับทาง Thomson Reuters ระดับปริญญาโท หลักสูตร [Business Software Development](https://bsd.cbs.chula.ac.th/) ของคณะบัญชี ภาควิชาสถิติ จุฬาลงกรณ์มหาวิทยาลัย ใช้เวลา 4 ปีกว่าจะจบ (ทำงานไปด้วย เรียนไปด้วยนี่ เหนื่อยมาก จริง ๆ) แต่ก็ได้อะไรเยอะมากระหว่างเรียน คณาจารย์และเพื่อน ๆ ดีมาก ๆ ครับ หลักสูตรนี้จริง ๆ แล้วสอนแนวคิดการทำ startup ตั้งแต่คำว่า startup ยังไม่ดังเลยครับ คือเน้นให้สร้างซอฟต์แวร์จากปัญหา (pain point) ก่อนเสมอ ซึ่งผมเชื่อว่าเป็นแนวทางที่ถูกต้อง

ระหว่างทำงานที่ Thomson Reuters เคยถูกส่งไปทำงานที่ [Thomsom Reuters London](https://www.thomsonreuters.com/en/locations/location-detail.html/london-south-colonnade-location) ประมาณ 3 เดือน เป็นประสบการณ์ที่ดีมาก ๆ หลังจากทำงานอยู่ที่ Thomson Reuters ประมาณ 7 ปี ก็เริ่มสนใจ web technology เลยออกมาอยู่บริษัททำเว็บเล็ก ๆ ชื่อ [openface](https://www.facebook.com/Openface.Internet/) เป็นบริษัท Joint Venture กับทาง Canada เป็นบริษัทเล็ก ๆ ที่อบอุ่นมาก ๆ ได้เรียนรู้การทำเว็บตั้งแต่ศูนย์ โดยเฉพาะ [Magento](https://magento.com/) ก็เลยหลงรัก web technology ตั้งแต่นั้นเป็นต้นมา

พออยู่ไปสักเกือบปี ได้รับการแนะนำโอกาสจากหัวหน้าเก่า (หัวหน้าผมนี่ระดับเทพ แทบทุกเรื่อง ไม่ว่าจะเป็น technical หรือ soft skill) ที่ Thomson Reuters ให้ไปร่วมทีมที่ปรึกษาของบริษัท [BridgeAsia](https://bridgeasiagroup.com/) มีโอกาสได้ไปช่วยลูกค้าทำ [Agile Transformation](https://startup.kbot.asia/posts/agile-from-experience/) อยู่ประมาณ 3 ปี เป็นประสบการณ์ที่ดีมาก ๆ ผ่านร้อน ผ่านหนาว ไปกับทีม dev ของลูกค้า มากมายหลายทีม ได้มีส่วนร่วมในการทำ startup หลาย ๆ ตัวที่ลูกค้าริเริ่ม พอรู้สึกอิ่มตัว หลังจากนั้นได้มีโอกาสไปร่วมงานกับ startup [aCommerce](https://acommerce.co.th/?) ในตำแหน่ง Solution Architect และได้กลับมาใช้ความรู้ Magento ลึก ๆ อีกครั้ง aCommerce เป็น startup ที่มี Business Model ดีมาก และเป็น international startup อย่างแท้จริง (เพื่อนร่วมงานผม มีชาวต่างชาติ หลากหลายมาก ฟิลิปปินส์ ฝรั่งเศส อังกฤษ อเมริกา ฯลฯ) ทำได้อยู่ประมาณเกือบปี ทาง BridgeAsia ก็เรียกกลับไปช่วยงาน Digital Transformation อีกครั้ง และก็ทำที่ BridgeAsia จนถึงปัจจุบันนี้ครับ

รอบนี้ มีการผันตัวเองมาอยู่ในสาย Healthcare Tech ด้วย ได้อยู่กับทีม [BridgeAsia Healthcare Ventures](https://bridgeasiagroup.com/healthcareventures) ที่ทำ AI/ML สำหรับ Computer Vision ทางการแพทย์ ได้ประสบการณ์หลากหลายมาก ๆ และสิ่งที่สร้าง ก็มี impact กับชีวิตผู้คนค่อนข้างจะมาก รู้สึกว่าตัวเองโชคดี ที่ได้มาอยู่ตรงนี้ แต่ก็ยังไม่ทิ้งงาน Agile / Digital Transformation นะครับ

---

#### Contact

หากมีคำถาม หรือ feedback

ติดต่อได้ที่อีเมล kamolcu แอด(@) gmail.com
